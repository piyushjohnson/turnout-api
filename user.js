const User = require('./models/user.js');
const Job = require('./models/user.js').job;
const authcontrol = require('./auth/authcontrol.js');
const express = require('express');
const user = express.Router({});

var uid,uuid;

user.post('/',isLoggedIn,function (req,res) {
    console.log(req.body);
    res.send('Welcome user');
});

user.post('/createjob',isLoggedIn,function (req,res) {
    console.log(req.body);
    var {name,title,type,inTime,outTime,status,latitude,longitude} = req.body;

    getUid(name,
        () => {
            User.findById(uid,function (err,user) {
                if(err) {console.log(err);return res.send('failed');}
                    console.log(title);
                    user.jobs.push({title: title,type: type,inTime: inTime,outTime: outTime,status: status,latitude: latitude,longitude: longitude});
                    user.save((err,user) => {
                        if(err) {console.log(err);return res.send('failed')}
                            return res.send('created job');
                    });
            });
        },
        () => {

        }
    );
    
});



user.post('/createuser',isLoggedIn,function (req,res) {
    console.log(req.body);
     new User(req.body).save((err,user) => {
         if(err) {console.log(err);return res.send('failed')}
         return res.send('created user');
     })
});

user.post('/getAll',isLoggedIn,function(req,res) {
    
    User.findById(uuid,function (err,user) {
        if(err) {console.log(err);return res.send('failed');}
        var fetchedJobs = [];
        user.jobs.forEach((job,index) => {
           fetchedJobs.push(job);
        });
        res.json({"jobs": fetchedJobs});
    });
})

function getAllUsers(res,prop)
{
    User.find(function(err,users) {
        if(err) {console.log(err);return res.send('failed');}
        var fetchedUsers = [];
        users.forEach((user,index) => {
            prop !== undefined ? fetchedUsers.push(user[prop]) : fetchedUsers.push(user);
        });
        res.json({"users":fetchedUsers});
    });
}

function getActiveUsers(res)
{
    User.find({active: true},function(err,users) {
        if(err) {console.log(err);return res.send('failed');}
        var fetchedUsers = [];
        users.forEach((user,index) => {
            fetchedUsers.push(user);
        });
        res.json({"users":fetchedUsers});
    });
}

function getVerifiedUsers(res)
{
    User.find({isVerified: true},function(err,users) {
        if(err) {console.log(err);return res.send('failed');}
        var fetchedUsers = [];
        users.forEach((user,index) => {
            fetchedUsers.push(user);
        });
        res.json({"users":fetchedUsers});
    });
}

function getAllJobs(res) {
    User.findById(uid,function (err,user) {
        if(err) {console.log(err);return res.send('failed');}
        var fetchedJobs = [];
        user.jobs.forEach((job,index) => {
           fetchedJobs.push(job);
        });
        res.json({"jobs": fetchedJobs});
    });
}

function getAssignedJobs(res) {
    User.findById(uid,function (err,user) {
        if(err) {console.log(err);return res.status(404).send('failed');}
        let jobs = user.jobs.filter((job) => {
            return job.type === 'assigned';
        });
        return res.json({"jobs":jobs});
    });
}

function getPendingJobs(res) {
    User.findById(uid,function (err,user) {
        if(err) {console.log(err);return res.status(404).send('failed');}
        let jobs = user.jobs.filter((job) => {
            return job.type === 'pending';
        });
        return res.json({"jobs":jobs});
    });
}

function getDoneJobs(res) {
    User.findById(uid,function (err,user) {
        if(err) {console.log(err);return res.status(404).send('failed');}
        let jobs = user.jobs.filter((job) => {
            return job.type === 'done';
        });
        return res.json({"jobs":jobs});
    });
}

function getAnyJobs(res) 
{

}


function getUid(name,success,error) {
    User.findOne({name: name},'name',(err,user) => {
        if(err) error();
        if(user !== null)
            uid = user._id
        success();
    });
}

user.post('/jobs/:type',isLoggedIn,function (req,res) {
    let type = req.params.type;
    let name = req.body.name;
    getUid(name, 
        function success(){
            console.log(uid);
            switch (type)
            {
                case "assigned":
                    getAssignedJobs(res);
                    break;
                case "pending":
                    getPendingJobs(res);
                    break;
                case "done":
                    getDoneJobs(res);
                    break;
                case "all":
                    getAllJobs(res)
                    break;
                default:
                    return res.status(400).end();
            }
        },
        function error(){

        }
    );
});

user.post('/users/:type',function (req,res) {
    let type = req.params.type;
    let prop = req.body.prop;
    switch(type)
    {
        case "all":
            getAllUsers(res,prop);
            break;
        case "active":
            getActiveUsers(res);
            break;
        case "verified":
            getVerifiedUsers(res);
            break;
            default:
            return res.status(400).end();
    }
});

function isLoggedIn(req, res, next) {
    // if(req.session.name !== undefined)
    // console.log("Checking Login: " + req.headers);
    console.log(req.session.auth_id);
    if (req.headers['x-access-token'] !== undefined || req.session.auth_id.token !== null) {
        // next();
        var token = req.headers['x-access-token'] || req.session.auth_id.token;
        console.log(token);
        var result = authcontrol.verifyToken(token, function (result) {
            if (result.auth === true) {
                authcontrol.decodeToken(token, (id) => {
                    if (id !== null) {
                        uuid = id;
                        return next();
                    }
                    else res.status(500).end();
                });

            }
            else if (result.auth === false) res.status(401).send('Unauthorized access');
        });
    }
    else
        res.status(403).send('No access token provided');
}

module.exports = user;