const jwt = require('jsonwebtoken');
const request = require('request');
const nodemailer = require('nodemailer');
const otplib = require('otplib');
const config = require('../config.js');

var secret ="$JG)#JF(#39JF932";

module.exports = {
    grantToken: (user_id) => {
        var token = jwt.sign({id: user_id},config.secret,{
            expiresIn: '1h'
        });

        return {auth: "true",token: token}
    },
    verifyToken: (token,res) => {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) { console.log(err);return res({auth: false, message: 'Token invalid'}); }
            else { console.log('Verified Token: ' + decoded.id);return res({auth: true, message: 'Token valid'}); }
        });
    },
    decodeToken: (token,res) => {
      let decodedToken = jwt.decode(token);
      if(decodedToken !== undefined) res(decodedToken.id);
      else res(null);
    },
    invalidateToken: (token,res) => {
        jwt.verify(token,config,secret,(err,decoded) =>{
           if(err) {console.log(err);return res({auth:false, message: 'Token invalid'});}
           else {
               console.log('logged out user ' + decoded.id);

           }
        });
    },
    generateOTP: (mob,res) => {
        if(mob !== undefined) {
            var formData = {
                authkey: '220983AvalOzfxlh5b26a057',
                sender: 'TESTSMS',
                mobile: mob,
                message: 'Verify your identity ##OTP##'
            };
            request.post({url:'http://control.msg91.com/api/sendotp.php',form: formData},function(err,json) {
                if(err) { console.log(err);return res(false); }
                let status = JSON.parse(json.body);
                console.log("SMS Sent: ",typeof status);
                if(status.type === "success")
                {
                    return res(true);
                }
                else if(status.type === "error")
                {
                    return res(false);
                }
            });
        }
    },
    verifyOTP: (code,mob,res) => {
        var formData = {
            authkey: '220983AvalOzfxlh5b26a057',
            mobile: mob,
            otp: code
        };

        request.post({url:'https://control.msg91.com/api/verifyRequestOTP.php',form: formData},function(err,json) {
            if(err) { console.log(err);return res(false) }
            let status = JSON.parse(json.body);
            if(status.type === "success")
            {
                return res(true);
            }
            else if(status.type === "error")
            {
                return res(false);
            }
        });
    },
    sendMail: (mailAddr,res) => {
        var token = otplib.authenticator.generate("$JG)#JF(#39JF932");

        var msg = `Hi user,<br/>Verify your email with this http://localhost:5134/verify?token=${token}`

        var tranporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{ user: 'piyushjohnson1998@gmail.com', pass: 'piyush#1998'}
        });

        var mail_options = {
            from: 'piyushjohnson1998@gmail.com',
            to: mailAddr,
            subject: 'Turnout Email Verification',
            text: msg
        };

        tranporter.sendMail(mail_options, function (err,res) {
            if(err) {console.log(err);return res(false)}
            else {console.log(res);return res(true)}
        });

        return res(true);
    },
    verifyMail: (token,res) => {
        let isValid = otplib.authenticator.check(token,"$JG)#JF(#39JF932");
        if(isValid)
            return res(true);
        else
            return res(false);
    }
};