const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

/** Job Schema */
var jobSchema = new Schema({
    title:  {type: String, index: true},
    type: { type: String },
    inTime: {type: String},
    outTime: {type: String},
    status: {type: Boolean},
    latitude: {type: Number},
    longitude: {type: Number}
},{_id: true});

/** User Schema */
var userSchema  = new Schema({
    name: { type: String, require: true, index: true, unique: true },
    password: { type: String, require: true},
    email: String,
    mob: {type: String,min: 10,max: 10},
    age: { type: Number, min: 18,max: 65},
    isVerified: {type: Boolean, default: false},
    active: {type: Boolean, default: false},
    logAttempts: Number,
    lastModified: { type: Date , default: Date.now() },
    jobs: [jobSchema]
},{_id: true});

/** User Schema methods */

userSchema.pre('save',function(next) {
   var user = this;
   console.log(user.password);

   if(!user.isModified('password')) return next();
   /*else {
       // var salt = bcrypt.genSaltSync(10);
       const password = user._doc.password;
       var hash = bcrypt.hashSync(password,10);
       user.password = hash;
       return next();
   }*/
   bcrypt.genSalt(10,(err,salt) => {
       if(err) return next(err);

       bcrypt.hash(user.password,salt,(err,hash) => {
           console.log(hash);
           user.password = hash;
           next();
       });
   });
});

userSchema.methods.validPassword = function(password,callback) {
    bcrypt.compare(password,this.password,(err,isMatch) => {
        if(err) return callback(err);
        callback(null,isMatch);
    });
};

userSchema.static('validUsername',function (username,callback) {
  return this.find({name: username},callback)
});

/** Instance of user model */

var user =  mongoose.model('user',userSchema);
var job = mongoose.model('job',jobSchema);

module.exports = user;

module.exports.job = job;