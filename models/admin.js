const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

/** Admin Schema */
var adminSchema  = new Schema({
    name: { type: String, require: true, index: true, unique: true },
    password: { type: String, require: true},
    email: String,
    isVerified: {type: Boolean,default: false},
    mailSent: {type: Boolean,default: false},
    mob: {type: String,min: 10,max: 10}
},{_id: true});

/** Admin Schema methods */

adminSchema.pre('save',function(next) {
    var admin = this;
    console.log(admin.password);
 
    if(!admin.isModified('password')) return next();
    /*else {
        // var salt = bcrypt.genSaltSync(10);
        const password = user._doc.password;
        var hash = bcrypt.hashSync(password,10);
        user.password = hash;
        return next();
    }*/
    bcrypt.genSalt(10,(err,salt) => {
        if(err) return next(err);
 
        bcrypt.hash(admin.password,salt,(err,hash) => {
            console.log(hash);
            admin.password = hash;
            next();
        });
    });
 });
 
 adminSchema.methods.validPassword = function(password,callback) {
     bcrypt.compare(password,this.password,(err,isMatch) => {
         if(err) return callback(err);
         callback(null,isMatch);
     });
 };

 
/** Instance of admin model */

var admin =  mongoose.model('admin',adminSchema);

module.exports = admin;