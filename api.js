const User = require('./models/user.js');
const authcontrol = require('./auth/authcontrol.js');
const express = require('express');
const api = express.Router({});


/** ROUTER: /api/user */
api.use('/user',require('./user.js'));

/** GET: / route */
api.get('/', function (req, res) {
    return res.status(200).send('API');
});

/** POST: login route */
api.post('/login', function (req, res) {
    if (req.body.name === undefined) {
        return res.send('failed');
    }
    User.findOne({name: req.body.name}, 'name password', function (err, user) {
        if (err) console.err(err);
        else if (user !== undefined) {
            user.validPassword(req.body.pass, (err, isMatch) => {
                if (isMatch) {
                    let username = user.name;
                    res.set('x-username',username);
                    console.log('Logged in as ' + username);
                    let gen_token = authcontrol.grantToken(user._id); //generating jwt token
                    res.set('x-access-token', gen_token.token); //setting token in header
                    return res.status(200).json('success');
                }
                else {
                    console.log('Failed to login as ' + req.body.name);
                    return res.status(200).json('failed')
                }
                ;
            });
        }
        else {
            console.log('Failed to login as ' + req.body.name);
            return res.status(200).json('failed')
        }
        ;
    });
});


/** POST: signup route */
api.post('/signup', function (req, res) {
    var newUser = new User({name: req.body.name, password: req.body.pass, email: req.body.email});
    newUser.save(function (err, user) {
        if (err) {
            console.log(err);
            res.status(500).send('failed');
        }
        else {
            console.log('Signed up as ' + user.name);
            return res.status(200).send('success');
        }
    });
});

/** POST: logout route */
api.post('/logout', isLoggedIn, (req, res) => {
    delete req.session;
    sess = null;
    res.set({"Clear-Site-Data": "*"})
    res.send('logout');
});

/** POST: authenticate route */
api.post('/authenticate', isLoggedIn, function (req, res) {
    var token = req.headers['x-access-token'] || req.session.auth_id;
        authcontrol.decodeToken(token, (id) => {
            if (id !== null) {
                console.log("User ID: ", id);
                User.findById(id, (err, user) => {
                    console.log("Username: ", user.name);
                    if (user.isVerified) {
                        return res.status(200).json('success');
                    }
                    authcontrol.generateOTP(user.mob, (status) => {
                        if (status)
                            return res.status(202).json('sent');
                        else
                            return res.status(500).json('failed');
                    });
                });
            }
            else
                res.status(400).end();
        });
});

/** POST: verify route */
api.post('/verify', isLoggedIn, (req, res) => {
    let otp = req.body.otp;

    if (otp !== null) {
        let token = req.headers['x-access-token'] || req.session.auth_id;
        if (token) {
            authcontrol.decodeToken(token, (id) => {
                if (id !== null) {
                    console.log("User ID: ", id);
                    User.findById(id, (err, user) => {
                        console.log("Username: ", user.name);
                        authcontrol.verifyOTP(otp, user.mob, (status) => {
                            if (status) {
                                user.isVerified = true;
                                user.save();
                                return res.status(200).json("success")
                            }
                            return res.json("failed");
                        });
                    });
                }
            });
        }
    }
    else
        res.status(400).end();
});


/** Utility methods */

function verifyUser(req, res, next) {
    // console.log("Verifying User: " + req.header);
}


function isLoggedIn(req, res, next) {
    // if(req.session.name !== undefined)
    // console.log("Checking Login: " + req.headers);
    if (req.headers['x-access-token'] !== undefined) {
        // next();
        var token = req.headers['x-access-token'] || req.session.auth_id;
        console.log(token);
        var result = authcontrol.verifyToken(token, function (result) {
            if (result.auth === true) next();
            else if (result.auth === false) res.status(401).send('Unauthorized access');
        });
    }
    else
        res.status(403).send('No access token provided');
}

module.exports = api;