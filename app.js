/** Instance of express app */

const express = require('express');
const morgan = require('morgan');
const process = require('process');
const compression = require('compression');
const zlib = require('zlib');
const app = express();
const api = require('./api');

/** Importing Modules */

const session = require('express-session');
const path = require('path');
const mongoose = require('mongoose');
const cookie_parser = require('cookie-parser');
const body_parser = require('body-parser');
const mongo_store = require('connect-mongo')(session);
const config = require('./config.js');

/** Database connection */

mongoose.connect('mongodb://premium11:premium11@ds247619.mlab.com:47619/test11',config.mongo_options)
        .then(() => console.log('Connected to online database'),
              (err) => console.log('Connection error: ' + err));
const db = mongoose.connection;
db.on('error',() => console.log('Unable connecting to databse'));


/** Setup all middlewares */

app.use(session({secret: 'expressjs',resave: false,saveUninitialized: false,cookie: {maxAge: 1 * 1000 * 60 * 60,httpOnly: false},store: new mongo_store({mongooseConnection: db,ttl: 1 * 24 * 60 * 60})}))
app.use(cookie_parser());
app.use(compression({level: 9,chunkSize: 16384,memLevel: 8,strategy:zlib.Z_RLE}));
app.use(express.static(path.join(__dirname,'public')));
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: false}));
app.use(morgan('dev'));

/** Setting view engine */

app.set('view engine','html');
app.engine('html',require('hbs').__express);
app.set('views',__dirname + '/views');

var sess = null;

/** Setup all routes */


app.use('/api',api);

require('./admin.js') (app,sess);

/** Start listening at port */

const httpServer = app.listen(config.port,function(){
    console.log('App started at ' + config.port);
});