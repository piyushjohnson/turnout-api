const Admin = require('./models/admin.js');
const authcontrol = require('./auth/authcontrol.js');
module.exports = function (app, sess) {

    /** ROUTER: /api/user */
    app.use('/user',require('./user.js'));

    app.use(function (req, res, next) {
        res.set('Expires', '0');
        res.set('Cache-Control', ['no-cache', 'no-store', 'must-revalidate']);
        next();
    });

    /** GET: /root route */
    app.get('/', function (req, res) {
        sess = req.session;
        console.log(sess.auth_id);
         if (sess.name === undefined) {
            res.status(200).render('index', {
                title: 'API',
                body: 'Welcome to API'
            });
            
        }
        else {
            res.status(200).render('index', {
                title: 'API',
                body: `Welcome , ${req.session.name}  <a href="/logout">Logout</a>`
            });
        }
    });

    /** GET: /login route */
    app.get('/login', function (req, res) {
        sess = req.session;
        if (sess.name !== undefined) {
            res.status(200).redirect('/dashboard');
        }
        else {
            res.status(200).render('login', {
                title: 'Login',
                body: 'Login here'
            });
        }
    });

    /** GET: signup route */
    app.get('/signup', function (req, res) {
        res.status(200).render('signup', {
            title: 'Signup',
            body: 'Become a member'
        });
    });

    /** POST: login route */
    app.post('/login', function (req, res) {
        sess = req.session;
        Admin.findOne({name: req.body.name}, 'name password email isVerified mailSent', function (err, admin) {
            if (err) console.err(err);
            else if (admin !== null) {
                // console.log(admin);
                if(admin.isVerified == true)
                {
                    admin.validPassword(req.body.pass, (err, isMatch) => {
                        if (isMatch) {
                            console.log('Logged in as ' + admin.name);
                            var gen_token = authcontrol.grantToken(admin._id);
                            req.session.auth_id = gen_token;
                            req.session.name = req.body.name;
                            return res.redirect(301, '/');
                        }
                        else return res.redirect(200, '/login');
                    });
                }
                else 
                {
                    if(admin.mailSent == false){
                        authcontrol.sendMail(admin.email,(isSent) => {
                            if(isSent) {
                                admin.mailSent = true;
                                admin.save();
                                console.log('Verification mail sent to ' + admin.email);
                                return res.status(200).send('Sent mail');
                            }
                            else  
                                return res.redirect(301,'/');
                        })
                    }
                    else
                        return res.status(401).send('Verify your email');
                }
            }
            else return res.status(200).send('failed');
        });
        // res.send('done');
    });


    /** POST: signup route */
    app.post('/signup', function (req, res) {
        let newAdmin = new Admin({name: req.body.name, password: req.body.pass, email: req.body.email});
        newAdmin.save(function (err, admin) {
            if (err) console.log(err);
            else {
                console.log('Signed up as ' + admin.name);
                return res.redirect(200, '/login');
            }
        });
        // res.status(200).redirect("/login");
    });

    /** GET: verify route */
    app.get('/verify',function (req,res) {
        console.log(req.query.token);
        authcontrol.verifyMail(req.query.token,(isValid) => {
            if(isValid)
                return res.status(200).send('Verification success');
            else
                return res.status(401).send('Verification failed');
        })
    });

    /** GET: logout route */
    app.get('/logout', (req, res) => {
        sess = req.session;
        if (sess.name) {
            req.session.destroy((error) => {
                if (error) console.log('Logout error');
                else res.redirect('/');
            });
        }
    });

    /** GET: dashboard route */
    app.get('/dashboard', isLoggedIn, function (req, res) {
        res.render('dashboard');
    });

};

/** Utility methods */
function isLoggedIn(req, res, next) {
    // if(req.headers['x-access-token'] !== undefined)
    if (req.session.name !== undefined) {
        // next();
        var token = req.headers['x-access-token'] || req.session.auth_id.token;
        var result = authcontrol.verifyToken(token, function (result) {
            if (result.auth === true) next();
            else if (result.auth === false) res.status(401).end('Unauthorized access');
        });
    }
    else
        res.status(401).end('No access token provided');
}
