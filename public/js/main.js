var search_box,map_conainer,location_selector_modal,sidenav,content_area,sidenav_items,user_form,job_form,actions_tab,active_users_list;
var breadcrumbs;
var loaded_pages = {
    users: false,
    reports: false,
    settings: false
}

$(document.body).ready(function () {

    search_box = $('#search_box');
    breadcrumbs = $('#current_tab');
    sidenav = $('.sidenav');
    content_area = $('#content_area');
    sidenav_items = $('#sidenav_items');


    $(window).on('load',function(event) {
            changeContent("users");
    })

    sidenav_items.children().each(function(index,item) {
        $(this).find('a').click((event) => {
            changeContent(event.target.id);
        })
    });

    search_box.keypress(function (event) {
        console.log(event.keyCode === 13 ? event.target.value: "");
    });

    setupAJAX();
    // setupListeners();
});

function changeContent(id)
{
    loaded_pages[id] = true;
    breadcrumbs.text(id);
    switch(id)
    {
        case "users":
            content_area.load('../layouts/user.html')
        break;
        case "reports":
            content_area.load('../layouts/reports.html');
        break;
        case "settings":
        break;
        default:
        break
    }
}

function loadData(res) {
    $.post('/user/users/all')
        .done((data) => {
            console.log(data);
            res(data);
        })
        .fail(() => {console.log('Failed to fetch users list')});
}

function setupData() {
    $.addTemplateFormatter({
        UpperCaseFormatter : function(value, template) {
                return value.toUpperCase();
            },
        LowerCaseFormatter : function(value, template) {
                return value.toLowerCase();
            },
        TotalJobsFormatter : function(value,tempalte) {
            return "Total Jobs " + value;
        },
        LastAssignedJob : function(value,template) {
            let i = value.length - 1;
            if(i == -1) return "";
            let job = value[i];
            return `<h6>${job.title}</h6> <time> ${job.inTime} - ${job.outTime} <time>`;
        },
        SameCaseFormatter : function(value, template) {
                if(template == "upper") {
                    return value.toUpperCase();
                } else {
                    return value.toLowerCase();
                }
            }
    });
    loadData((data) => {
        active_users_list.loadTemplate("#item_template",data.users);
    });
}

function setupListeners() {

    active_users_list.children().each(function(index) {
        $(this).find('a>i').click((event) => {
            console.log($(this));
        });
     });
    
    // $(window).on('hashchange',function(event) {
    //     var hash = location.hash.substr(1);
    //     // changeContent(hash);
    // })

    location_selector_modal.modal({
        onOpenStart: () => {location_selector_modal.load('../layouts/location.html')},
        onCloseStart: () => {$('#latitude').val(lat).focus();$('#longitude').val(lng).focus();return true;}
    })

    user_form.submit(function (event) {
        let data = user_form.serializeObject();
        $.post('/user/createuser', data)
            .done((data) => {console.log('success: ' + data);Materialize.Toast({html:"User created"});})
            .fail(() => {console.log('failed');M.Toast({html:"Failed to create"});});
        event.preventDefault();
    });

    job_form.submit(function(event) {
        let data = job_form.serializeObject();
        console.log(data);
        $.post('/user/createjob',data)
            .done((data) => {console.log('success: ' + data);})
            .fail(() => {console.log('failed');})
        event.preventDefault();
    });

}

function setupAJAX() {
    $.ajaxSetup({
        url: "http://localhost:5134"
    })
}